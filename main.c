#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "so2ext2.h"

#define BLOCK_SIZE 1024
#define EXT2_ROOT_INO 2

struct metadata
{
    int inodesInGroup;
    int inodesInBlock;
    int blocksInGroup;
    int inodeTableBlock;
};

void readSuperBlock(int file, struct ext2_super_block* superBlock);
void readGroupDesc(int file, struct ext2_group_desc* groupDesc);
void readInode(int inodeNumber, int file, struct ext2_inode* inode, struct metadata* metadata);
void readDir(struct ext2_inode* inode, int file, struct metadata* metadata, int depth, int maxDepth);

int main(int argc, char* argv[])
{
    int maxDepth = 0;

    if (argc < 2)
    {
        fprintf(stderr, "Image name is missing\n");
        return EXIT_FAILURE;
    }
    else if (argc == 3)
    {
        maxDepth = atoi(argv[2]);
    }

    int file = open(argv[1], O_RDONLY);
    if (file == -1)
    {
        fprintf(stderr, "Could not open image\n");
        return EXIT_FAILURE;
    }

    struct stat fileInfo;
    fstat(file, &fileInfo);

    struct ext2_super_block superBlock;
    readSuperBlock(file, &superBlock);

    struct ext2_group_desc groupDesc;
    readGroupDesc(file, &groupDesc);

    struct metadata metadata;
    metadata.inodesInGroup = superBlock.s_inodes_per_group;
    metadata.inodesInBlock = BLOCK_SIZE / sizeof(struct ext2_inode);
    metadata.blocksInGroup = superBlock.s_blocks_per_group;
    metadata.inodeTableBlock = groupDesc.bg_inode_table;

    struct ext2_inode inode;
    readInode(EXT2_ROOT_INO, file, &inode, &metadata);

    if (S_ISDIR(inode.i_mode))
        readDir(&inode, file, &metadata, 0, maxDepth);

    close(file);
    return EXIT_SUCCESS;
}

void readSuperBlock(int file, struct ext2_super_block* superBlock)
{
    lseek(file, BLOCK_SIZE, SEEK_SET);
    read(file, superBlock, sizeof(struct ext2_super_block));
}

void readGroupDesc(int file, struct ext2_group_desc* groupDesc)
{
    lseek(file, 2 * BLOCK_SIZE, SEEK_SET);
    read(file, groupDesc, sizeof(struct ext2_group_desc));
}

void readInode(int inodeNumber, int file, struct ext2_inode* inode, struct metadata* metadata)
{
    int blockGroupNum = (inodeNumber - 1) / metadata->inodesInGroup;
    int localInodeIndex = (inodeNumber - 1) % metadata->inodesInGroup;
    int inodeBlockNum = localInodeIndex / metadata->inodesInBlock + metadata->inodeTableBlock;
    int inodeOffset = (localInodeIndex % metadata->inodesInBlock) * sizeof(struct ext2_inode);
    int blockGroupOffset = blockGroupNum * metadata->blocksInGroup * BLOCK_SIZE;

    lseek(file, blockGroupOffset + inodeBlockNum * BLOCK_SIZE + inodeOffset, SEEK_SET);
    read(file, inode, sizeof(struct ext2_inode));
}

void readDir(struct ext2_inode* inode, int file, struct metadata* metadata, int depth, int maxDepth)
{
    for (int i = 0; i < EXT2_NDIR_BLOCKS; i++)
    {
        if (inode->i_block[i] == 0)
            continue;

        unsigned int offset = 0;
        while (offset < inode->i_size)
        {
            struct ext2_dir_entry dirEntry;
            lseek(file, inode->i_block[i] * BLOCK_SIZE + offset, SEEK_SET);
            read(file, &dirEntry, sizeof(struct ext2_dir_entry));

            struct ext2_inode entryInode;
            readInode(dirEntry.inode, file, &entryInode, metadata);

            char* padding = "\t\t\t\t\t\t";
            printf("%.*s", depth, padding);

            if (S_ISREG(entryInode.i_mode))
                printf("file\t");
            else if (S_ISDIR(entryInode.i_mode))
                printf("dir\t");
            else if (S_ISCHR(entryInode.i_mode))
                printf("character\t");
            else if (S_ISBLK(entryInode.i_mode))
                printf("block\t");
            else if (S_ISFIFO(entryInode.i_mode))
                printf("pipe\t");
            else if (S_ISSOCK(entryInode.i_mode))
                printf("socket\t");
            else if (S_ISLNK(entryInode.i_mode))
                printf("link\t");
            else
                printf("\t");

            int permissions = entryInode.i_mode & 0x1FF;
            printf("%o\t", permissions);

            printf("%d\t", entryInode.i_size);

            char name[EXT2_NAME_LEN];
            snprintf(name, dirEntry.name_len + 1, "%s", dirEntry.name);
            printf("%s\n", name);

            if (S_ISDIR(entryInode.i_mode)
                && strncmp(dirEntry.name, ".", dirEntry.name_len) != 0
                && strncmp(dirEntry.name, "..", dirEntry.name_len) != 0
                && strncmp(dirEntry.name, "lost+found", dirEntry.name_len) != 0
                && depth < maxDepth)
            {
                printf("%.*s\\\n", depth, padding);
                readDir(&entryInode, file, metadata, depth + 1, maxDepth);
                printf("%.*s/\n", depth, padding);
            }

            offset += dirEntry.rec_len;
        }
    }
}
